import sqlite3


def count_users(conn: sqlite3.Connection) -> int:
    """
    Count the total number of rows in the "users" table from an SQLite database.

    :param conn: Connection to the SQLite database

    :return: Number of rows in the "users" table

    :raises sqlite3.Error: When no "users" table is present in the database
    """

    cur = conn.cursor()
    r = cur.execute("""SELECT COUNT(*) FROM users""")
    v = r.fetchone()

    return v[0]
