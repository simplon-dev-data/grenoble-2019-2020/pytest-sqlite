# `pytest` + `sqlite3` = 🚀

> Or how to test some code using databases!

This is a sample projet demonstrating how to test some code using an SQLite database using the `sqlite3` module,
using the concept of _test fixtures_ (i.e. test context / dependencies) provided by the `pytest` testing framework.

The code to be tested is within the [app module](app.py).
The associated testing code is within the [test_app module](test_app.py).

Generating testing data is performed using the `faker` package.

## Usage

1. `pipenv install -d`
2. `pipenv shell`
3. `pytest -v`

## Documentation

- [pytest](https://docs.pytest.org)
- [sqlite3](https://docs.python.org/3/library/sqlite3.html)
- [faker](https://faker.readthedocs.io)
