import sqlite3
import pytest
import faker
import app


# initialize the faker package
fake = faker.Faker()


@pytest.fixture(scope="session")
def db():
    """
    This fixture initializes an `sqlite3.Connection` with an in-memory database.
    On termination, the database connection is closed.

    :yield: sqlite3.Connection
    """

    conn = sqlite3.connect(":memory:")

    yield conn

    conn.close()


@pytest.fixture(scope="function")
def users_table(db):
    """
    This fixture depends on an `sqlite3.Connection` to create table named "users".
    On termination, the "users" table is dropped.

    :yield: None
    """

    cur = db.cursor()
    cur.execute("""
        CREATE TABLE users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name TEXT,
            last_name TEXT
        )
    """)
    db.commit()

    yield

    cur.execute("""DROP TABLE users""")
    db.commit()


@pytest.fixture(scope="function")
def sample_users(db, users_table):
    """
    This fixture depends on an `sqlite3.Connection` with a "users" table to insert some sample users into it.

    :yield: list[tuple(first_name, last_name)]
    """

    fake_users = [(fake.first_name(), fake.last_name()) for i in range(100)]

    cur = db.cursor()
    for u in fake_users:
        cur.execute("""
            INSERT INTO users (first_name, last_name)
            VALUES (?, ?)
        """, (u[0], u[1]))
    db.commit()

    yield fake_users


def test_count_users_no_table(db):
    """
    Test that the `app.count_users` function raises an error
    when no "users" table is present in the database.

    This tests depends on the `db` fixture.
    """

    with pytest.raises(sqlite3.Error):
        app.count_users(db)


def test_count_users_empty(db, users_table):
    """
    Test that the `app.count_users` function returns `0`
    when no rows are present in the "users" table.

    This tests depends on the `db` and `users_table` fixtures.
    """

    num_users = app.count_users(db)
    assert num_users == 0


def test_count_users_samples(db, users_table, sample_users):
    """
    Test that the `app.count_users` function returns the right number of users
    when rows are already present in the "users" table.

    This tests depends on the `db`, `users_table` and `sample_users` fixtures.
    """

    expected_num = len(sample_users)
    num_users = app.count_users(db)
    assert num_users == expected_num
